package com.myicpc.model.quest;

import com.myicpc.model.IdGeneratedObject;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(initialValue = 1, allocationSize = 1, name = "idgen", sequenceName = "analysis_id_seq")
public class AnalysisItem extends IdGeneratedObject {
    private static final long serialVersionUID = 493679370882579754L;

    double lat;
    double lon;
    String browser;
    String version;
    String device;
    double time;

    public AnalysisItem(float lat, float lon, String browser, String version, String device, long time) {
        this.lat = lat;
        this.lon = lon;
        this.browser = browser;
        this.version = version;
        this.device = device;
        this.time = time;
    }

    public AnalysisItem(){

    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }
}
