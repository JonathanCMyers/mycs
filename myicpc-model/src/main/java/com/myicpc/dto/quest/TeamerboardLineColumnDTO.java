package com.myicpc.dto.quest;

import com.myicpc.model.quest.QuestSubmission;
import com.myicpc.model.quest.QuestSubmission.QuestSubmissionState;

import java.io.Serializable;

/**
 * @author Tomas
 */
public class TeamerboardLineColumnDTO implements Serializable {

	private static final long serialVersionUID = -281379680806772894L;
	private final Long teamInfoId;
	private final String teamInfoName;
	private final Long questChallengeId;
	private final String questChallengeName;
	private final int questPoints;
	private final QuestSubmission.QuestSubmissionState submissionState;
	private final String reasonToReject;
	private final Long questSubmissionId;

	public TeamerboardLineColumnDTO(Long teamInfoId, String teamInfoName, Long questChallengeId, String questChallengeName,
			int questPoints, QuestSubmissionState submissionState, String reasonToReject, Long questSubmissionId) {
		super();
		this.teamInfoId = teamInfoId;
		this.teamInfoName = teamInfoName;
		this.questChallengeId = questChallengeId;
		this.questChallengeName = questChallengeName;
		this.questPoints = questPoints;
		this.submissionState = submissionState;
		this.reasonToReject = reasonToReject;
		this.questSubmissionId = questSubmissionId;
	}

	public Long getQuestChallengeId() {
		return questChallengeId;
	}

	public QuestSubmission.QuestSubmissionState getSubmissionState() {
		return submissionState;
	}

	public String getReasonToReject() {
		return reasonToReject;
	}

	public Long getTeamInfoId() {
		return teamInfoId;
	}

	public String getTeamInfoName() {
		return teamInfoName;
	}

	public String getQuestChallengeName() {
		return questChallengeName;
	}

	public int getQuestPoints() {
		return questPoints;
	}

	public Long getQuestSubmissionId() {
		return questSubmissionId;
	}

}
