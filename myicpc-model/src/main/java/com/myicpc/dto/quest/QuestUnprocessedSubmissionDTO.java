package com.myicpc.dto.quest;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Robert Cepa on 5/9/17.
 */
public class QuestUnprocessedSubmissionDTO implements Serializable {
    private static final long serialVersionUID = 3645904779148335413L;

    private Long submissionId;
    private Long challengeId;
    private String username;
    private Date notificationDate;
    private String challengeName;
    private Date challengeStart;
    private Date challengeEnd;

    public QuestUnprocessedSubmissionDTO(Long submissionId, Long challengeId, String username, Date notificationDate, String challengeName, Date challengeStart, Date challengeEnd) {
        this.submissionId = submissionId;
        this.challengeId = challengeId;
        this.username = username;
        this.notificationDate = notificationDate;
        this.challengeName = challengeName;
        this.challengeStart = challengeStart;
        this.challengeEnd = challengeEnd;
    }

    public Long getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(Long submissionId) {
        this.submissionId = submissionId;
    }

    public Long getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(Long challengeId) {
        this.challengeId = challengeId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getChallengeName() {
        return challengeName;
    }

    public void setChallengeName(String challengeName) {
        this.challengeName = challengeName;
    }

    public Date getChallengeStart() {
        return challengeStart;
    }

    public void setChallengeStart(Date challengeStart) {
        this.challengeStart = challengeStart;
    }

    public Date getChallengeEnd() {
        return challengeEnd;
    }

    public void setChallengeEnd(Date challengeEnd) {
        this.challengeEnd = challengeEnd;
    }
}
