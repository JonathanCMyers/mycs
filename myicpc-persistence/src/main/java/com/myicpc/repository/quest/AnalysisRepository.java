package com.myicpc.repository.quest;

import com.myicpc.model.quest.AnalysisItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnalysisRepository extends JpaRepository<AnalysisItem, Long>  {

}
