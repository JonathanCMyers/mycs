package com.myicpc.master.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * @author Roman Smetana
 */
@Service
public class JMSSenderService {

    private static final String SOCIAL_NOTIFICATION_QUEUE = "java:/jms/queue/SocialNotificationQueue";
    private static final String QUEST_NOTIFICATION_QUEUE = "java:/jms/queue/QuestNotificationQueue";
    private static final String SCHEDULE_NOTIFICATION_QUEUE = "java:/jms/queue/ScheduleNotificationQueue";
    private static final String CM_SYNCHRONIZATION_QUEUE = "java:/jms/queue/CMSynchronizationQueue";
    private static final String CODE_INSIGHT_QUEUE = "java:/jms/queue/CodeInsightQueue";

    @Autowired
    @Qualifier("jmsQueueTemplate")
    private JmsTemplate jmsQueueTemplate;

    public void sendSocialMessage(Serializable object) {
        sendMessage(SOCIAL_NOTIFICATION_QUEUE, object);
    }

    public void sendQuestMessage(Serializable object) {
        sendMessage(QUEST_NOTIFICATION_QUEUE, object);
    }

    public void sendScheduleMessage(Serializable object) {
        sendMessage(SCHEDULE_NOTIFICATION_QUEUE, object);
    }

    public void sendCMSynchronizationMessage(Serializable object) {
        sendMessage(CM_SYNCHRONIZATION_QUEUE, object);
    }

    public void sendCodeInsightMessage(Serializable object) {
        sendMessage(CODE_INSIGHT_QUEUE, object);
    }

    private void sendMessage(final String queue, final Serializable object) {
        jmsQueueTemplate.send(queue, session -> {
            return session.createObjectMessage(object);
        });
//        System.out.println("Sending message to " + queue + " object " + object);
    }

}
