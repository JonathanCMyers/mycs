package com.myicpc.master.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Roman Smetana
 */
@Configuration
@EnableScheduling
@ComponentScan("com.myicpc.master")
public class ServiceConfig {
}
