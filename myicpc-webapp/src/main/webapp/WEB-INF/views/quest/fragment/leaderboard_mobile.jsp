<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>
<table class="table table-striped table-condensed">
  <thead>
  <tr class="leaderboard-header-row">
    <th><spring:message code="quest.leaderboard.rank" /></th>
    <th ><spring:message code="quest.leaderboard.participant" /></th>
    <th class="text-center"><spring:message code="quest.leaderboard.points" /></th>
    <th class="text-center"><spring:message code="quest.leaderboard.numSolved" /></th>
  </tr>
  </thead>
  <tbody>
  <c:forEach var="questParticipant" items="${participantsDTO}" varStatus="status">
    <tr style="position: relative">
      <td>${questParticipant.key}</td>
      <td><a href="<spring:url value="${contestURL}/people/${questParticipant.value.contestParticipant.id}" />">${questParticipant.value.contestParticipant.firstname}</a></td>
      <td class="text-center">${questParticipant.value.totalPoints}</td>
      <td class="text-center">${questParticipant.value.acceptedSubmissions}</td>
    </tr>
  </c:forEach>
  </tbody>
</table>