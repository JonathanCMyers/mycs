<script type="application/javascript">
 $(function() {
    <c:forEach var="challenge" items="${challenges}">
    <c:if test="${not empty challenge.secondsToEndDate and challenge.secondsToEndDate gt 0}">
    var timediff${challenge.id} = ${challenge.secondsToEndDate};
    flashChallengeTime${challenge.id}();
    var flashChallengeInterval${challenge.id} = setInterval(flashChallengeTime${challenge.id}, 1000);

    function flashChallengeTime${challenge.id}() {
        if (timediff${challenge.id} >= 0) {
            $(".challenge-${challenge.id}-countdown").html(convertSecondsToPretty(timediff${challenge.id}));
            timediff${challenge.id} -= 1;
        } else {
            $(".challenge-${challenge.id}-countdown").html('<spring:message code="quest.challenge.over" />');
            clearInterval(flashChallengeInterval${challenge.id});

        }
    }
    </c:if>
    </c:forEach>
});
</script>