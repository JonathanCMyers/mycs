<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>

<t:template>


    <jsp:attribute name="head">
        <script src="<c:url value='/js/myicpc/controllers/questLeaderboard.js'/>" defer></script>
    </jsp:attribute>
    <jsp:attribute name="headline">
        <spring:message code="quest.teamerboard" />
    </jsp:attribute>
    <jsp:attribute name="title">
        <spring:message code="quest.teamerboard" />
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="<c:url value='/js/myicpc/questLeaderboard.js'/>" defer></script>
        <script type="application/javascript">
            function updateLeaderboard() {
                var $leaderboardNotification = $("#leaderboard-notification");
                $leaderboardNotification.removeClass('hidden');
            }

            $(function() {
                startSubscribe('${r.contextPath}', '${contest.code}', 'quest', updateLeaderboard, null);

                $("#leaderboard-notification a").click(function() {
                    var $leaderboardNotification = $("#leaderboard-notification");
                    $leaderboardNotification.append('<span class="fa fa-spinner fa-spin"></span>');
                    $.get("<spring:url value="${contestURL}/quest/teamerboard/update" />", function(data) {
                        $("#mainLeaderboard").html(data);
                        $leaderboardNotification.addClass('hidden');
                        $leaderboardNotification.find(':last-child').remove();
                    })
                });
            })
        </script>
    </jsp:attribute>

    <jsp:body>
        <%--<%@ include file="/WEB-INF/views/quest/fragment/questInfo.jsp" %> --%>
        <c:if test="${not empty teamerboard}">
   
        <c:set var="disableRole" value ="true"/>
        <c:set var="subDirectoryLink" value ="team"/>

            <div id="leaderboard-notification" class="alert alert-warning alert-dismissible hidden">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <spring:message code="quest.leaderboard.newSubmission" /> <a href="javascript:void(0)" class="alert-link"><spring:message code="quest.leaderboard.newSubmission.refresh" /></a>
            </div>

            <div id="mainLeaderboard" style="width: 100%; overflow-y: auto">
                <%@include file="/WEB-INF/views/quest/fragment/leaderboard_desktop.jsp" %>
            </div>
        </c:if>

        <c:if test="${empty teamerboard}">
            <div class="no-items-available">
                <spring:message code="quest.leaderboard.noResult" />
            </div>
        </c:if>

    </jsp:body>
</t:template>
