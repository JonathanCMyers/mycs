<%@ include file="/WEB-INF/views/includes/taglibs.jsp"%>
<t:modalWindow id="participateInChallenge">
    <jsp:attribute name="title">
        <spring:message code="quest.participate" />
    </jsp:attribute>

    <jsp:body>
        <t:alert>
            <spring:message code="quest.challenge.participate.hint1" />
            <strong>#${contest.hashtag}</strong>
            <strong id="challengeModalHashtag"></strong>
            <spring:message code="quest.challenge.participate.hint2" />
        </t:alert>

        <div class="clearfix text-center">
            <div class="col-sm-6 quest-participate">
                <a id="twitterParticipateInChallenge" href="#" class="block thumbnail twitter-color">
                    <t:faIcon icon="twitter" />
                    <span><spring:message code="twitter" /></span>
                </a>
            </div>
            <!--<div class="col-sm-6 quest-participate">
                <a href="https://vine.co/" class="block thumbnail vine-color">
                    <t:faIcon icon="vine" />
                    <span><spring:message code="vine" /></span>
                </a>
            </div>
            <div class="col-sm-6 quest-participate">
                <a href="https://instagram.com/" class="block thumbnail instagram-color">
                    <t:faIcon icon="instagram" />
                    <span><spring:message code="instagram" /></span>
                </a>
            </div>-->
        </div>
    </jsp:body>

</t:modalWindow>

<script type="text/javascript">
    function showParticipateChallenge(hashtag, title) {

        getLocation();

        function getLocation() {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(sendPosition);
            } else {

                console.log("Geolocation is not supported by this browser.");
            }
        }

        function sendPosition(position) {

            var browser=get_browser();
            browser.device = navigator.userAgent

            $.ajax({
                url: "${contestURL}/gps",
                type: "get",
                data: {
                    "lat": position.coords.latitude,
                    "lon":position.coords.longitude,
                    "browser":browser.name,
                    "bver":browser.version,
                    "dev":browser.device
                },
                success: function(response) {
                    console.log("Geolocation sent.");
                },
                error: function(xhr) {
                    console.log("Geolocation not sent.");r
                }
            });


<%--//            $.post("${contestURL}/gps",--%>
<%--//                {--%>
<%--//                    lat: position.coords.latitude,--%>
<%--//                    lon: position.coords.longitude,--%>
<%--//                    browser: browser.name,--%>
<%--//                    bver: browser.version,--%>
<%--//                    dev: browser.device--%>
<%--//                },--%>
<%--//                function(data, status){--%>
<%--//                    console.log("Geolocation sent.");--%>
<%--//                });--%>
        }


        // code snippet by: Hermann Ingjaldsson | https://stackoverflow.com/questions/5916900/how-can-you-detect-the-version-of-a-browser
        function get_browser() {
            var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
                return {name:'IE',version:(tem[1]||'')};
            }
            if(M[1]==='Chrome'){
                tem=ua.match(/\bOPR|Edge\/(\d+)/)
                if(tem!=null)   {return {name:'Opera', version:tem[1]};}
            }
            M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
            return {
                name: M[0],
                version: M[1],
                device: null
            };
        }

        $("#participateInChallengeLabel").html('<spring:message code="quest.participate" /> ' + title);
        <c:if test="${currentDevice.normal}">
        $("#twitterParticipateInChallenge").prop("href", 'http://twitter.com/intent/tweet?hashtags='+hashtag+',${contest.hashtag}');
        </c:if>
        <c:if test="${not currentDevice.normal}">
        $("#twitterParticipateInChallenge").prop("href", 'twitter://post?message=%23'+hashtag+' %23${contest.hashtag}');
        </c:if>
        $("#challengeModalHashtag").html('#' + hashtag);

        $("#participateInChallenge").modal('show');
    }
</script>