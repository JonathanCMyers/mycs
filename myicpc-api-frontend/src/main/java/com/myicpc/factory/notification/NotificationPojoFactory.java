package com.myicpc.factory.notification;

import com.myicpc.commons.utils.TimeUtils;
import com.myicpc.model.social.Notification;
import com.myicpc.pojo.notification.NotificationPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Cepa on 4/13/17.
 */
public class NotificationPojoFactory {

    public static NotificationPojo notificationPojoFactory(Notification notification) {
        NotificationPojo notificationPojo = new NotificationPojo();
        notificationPojo.setId(notification.getId());
        notificationPojo.setExternalId(notification.getExternalId());
        notificationPojo.setType(notification.getNotificationType().getCode());
        notificationPojo.setTitle(notification.getTitle());
        notificationPojo.setBody(notification.getBody());
        notificationPojo.setAuthorName(notification.getAuthorName());
        notificationPojo.setUrl(notification.getUrl());
        notificationPojo.setImageUrl(notification.getImageUrl());
        notificationPojo.setVideoUrl(notification.getVideoUrl());
        notificationPojo.setThumbnailUrl(notification.getThumbnailUrl());
        notificationPojo.setEntityId(notification.getEntityId());
        notificationPojo.setCode(notification.getCode());
        notificationPojo.setTimestamp(TimeUtils.convertUTCDateToLocal(notification.getTimestamp(), notification.getContest().getTimeDifference()));
        notificationPojo.setProfileUrl(notification.getProfilePictureUrl());
        return notificationPojo;
    }

    public static List<NotificationPojo> notificationPojoListFactory(List<Notification> notifications) {
        List<NotificationPojo> notificationPojos = new ArrayList<>();
        for (Notification notification : notifications) {
            notificationPojos.add(notificationPojoFactory(notification));
        }
        return notificationPojos;
    }
}
