package com.myicpc.rest.contest;

import com.myicpc.enums.CodeSuccess;
import com.myicpc.factory.contest.ContestPojoFactory;
import com.myicpc.pojo.JsonResponse;
import com.myicpc.pojo.contest.ContestPojo;
import com.myicpc.service.contest.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Robert Cepa on 4/12/17.
 */
@RestController
@RequestMapping("/contests")
public class ContestRestController {
    @Autowired
    private ContestService contestService;

    /**
     * Gives JSON of all visible contests.
     *
     * @return List of contests converted to JSON.
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public JsonResponse<List<ContestPojo>> contests() {
        return new JsonResponse<>(CodeSuccess.OK, ContestPojoFactory.contestPojoListFactory(contestService.getVisibleContests()));
    }
}
