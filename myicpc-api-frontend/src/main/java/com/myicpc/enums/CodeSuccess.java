package com.myicpc.enums;

/**
 * Created by Robert Cepa on 15/04/2017.
 */
public enum CodeSuccess implements HTTPStatusCode {
    OK(200, "Success"),
    CREATED(201, "Created"),
    ACCEPTED(202, "Accepted"),
    NO_CONTENT(204, "No content");

    /**
     * The HTTP status code
     */
    private final int code;

    /**
     * Brief desciption of the given status code
     */
    private final String label;

    CodeSuccess(final int code, final String label) {
        this.code = code;
        this.label = label;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
