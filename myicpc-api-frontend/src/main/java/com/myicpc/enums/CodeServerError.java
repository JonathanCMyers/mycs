package com.myicpc.enums;

/**
 * Created by Robert Cepa on 15/04/2017.
 */
public enum CodeServerError implements HTTPStatusCode {
    INTERNAL_SERVER_ERROR(500, "Internal server error");

    /**
     * The HTTP status code
     */
    private final int code;

    /**
     * Brief desciption of the given status code
     */
    private final String label;

    CodeServerError(final int code, final String label) {
        this.code = code;
        this.label = label;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
